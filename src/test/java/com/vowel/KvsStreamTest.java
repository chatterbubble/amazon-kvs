package com.vowel;

import com.amazonaws.auth.AWSCredentialsProvider;
import com.amazonaws.auth.DefaultAWSCredentialsProviderChain;
import com.amazonaws.client.builder.AwsClientBuilder;
import com.amazonaws.kinesisvideo.parser.examples.PutMediaWorker;
import com.amazonaws.kinesisvideo.parser.examples.StreamOps;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.kinesisvideo.AmazonKinesisVideo;
import com.amazonaws.services.kinesisvideo.AmazonKinesisVideoClientBuilder;
import com.amazonaws.services.kinesisvideo.AmazonKinesisVideoMediaClientBuilder;
import com.amazonaws.services.kinesisvideo.model.*;
import com.google.common.util.concurrent.Futures;
import com.google.common.util.concurrent.ListenableFuture;
import com.google.common.util.concurrent.ListeningExecutorService;
import com.google.common.util.concurrent.MoreExecutors;
import org.awaitility.Awaitility;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.net.URI;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Optional;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.function.Function;

public class KvsStreamTest {

    private static final Regions REGION = Regions.US_EAST_1;
    public static final String STREAM_NAME = "kvs-test-stream";

    private final AWSCredentialsProvider awsCredentialsProvider = DefaultAWSCredentialsProviderChain.getInstance();
    private ListeningExecutorService executorService;
    private AmazonKinesisVideo kinesisVideo;

    @BeforeEach
    public void init() throws Exception {
        executorService = MoreExecutors.listeningDecorator(Executors.newSingleThreadExecutor());
        kinesisVideo = AmazonKinesisVideoClientBuilder.standard().withRegion(REGION).withCredentials(awsCredentialsProvider).build();
    }

    @AfterEach
    public void cleanup() {
        executorService.shutdownNow();
    }

    @FunctionalInterface
    public interface ThrowingFunction<T, R, E extends Throwable> {
        R apply(T t) throws E;

        static <T, R, E extends Throwable> Function<T, R> unchecked(ThrowingFunction<T, R, E> f) {
            return t -> {
                try {
                    return f.apply(t);
                } catch (Throwable e) {
                    throw new RuntimeException(e);
                }
            };
        }
    }

    private ListenableFuture<?> uploadMedia(AmazonKinesisVideo kinesisVideo, String streamName, Optional<InputStream> mediaStream) throws InterruptedException {
        StreamOps streamOps = StreamOps.builder().credentialsProvider(awsCredentialsProvider).region(REGION).streamName(streamName).build();
        streamOps.recreateStreamIfNecessary();
        mediaStream.map(ThrowingFunction.unchecked(input -> {
            input.mark(Integer.MAX_VALUE);
            PutMediaWorker worker;
            for (int i = 0; i < 200; i += worker.getNumFragmentsPersisted()) {
                worker = PutMediaWorker.create(REGION, awsCredentialsProvider, streamName, input, kinesisVideo);
                executorService.submit(worker).get();
                input.reset();
            }
            return null;
        }));
        return Futures.immediateFuture(null);
    }

    private <R> ListenableFuture<R> downloadMedia(AmazonKinesisVideo kinesisVideo, String streamName, Function<GetMediaResult, R> func) {
        return executorService.submit(() -> {
            GetDataEndpointRequest endpointRequest = new GetDataEndpointRequest().withAPIName(APIName.GET_MEDIA).withStreamName(streamName);
            var endPoint = kinesisVideo.getDataEndpoint(endpointRequest).getDataEndpoint();
            var mediaClient = AmazonKinesisVideoMediaClientBuilder.standard()
                    .withEndpointConfiguration(new AwsClientBuilder.EndpointConfiguration(endPoint, REGION.getName()))
                    .withCredentials(awsCredentialsProvider).build();
            try {
                var result = mediaClient.getMedia(new GetMediaRequest().withStreamName(streamName).withStartSelector(new StartSelector().withStartSelectorType("EARLIEST")));
                return func.apply(result);
            } finally {
                mediaClient.shutdown();
            }
        });
    }

    @Test
    public void closingVideoStreamWithoutError() throws Exception {
        uploadMedia(kinesisVideo, STREAM_NAME, Optional.empty()).get();

        var future = downloadMedia(kinesisVideo, STREAM_NAME, ThrowingFunction.unchecked(r -> {
            r.getPayload().close();
            return null;
        }));

        Awaitility.await()
                .dontCatchUncaughtExceptions()
                .atMost(10, TimeUnit.SECONDS)
                .pollInterval(1, TimeUnit.SECONDS)
                .until(future::isDone);
    }

    @Test
    public void closingLongVideoStreamTakesLongTime() throws Exception {
        URI resourceURI = this.getClass().getClassLoader().getResource("bezos_vogels.mkv").toURI();
        InputStream inputStream = new ByteArrayInputStream(Files.readAllBytes(Paths.get(resourceURI)));
        uploadMedia(kinesisVideo, STREAM_NAME, Optional.of(inputStream)).get();

        var future = downloadMedia(kinesisVideo, STREAM_NAME, ThrowingFunction.unchecked(r -> {
            r.getPayload().close();
            return null;
        }));

        Awaitility.await()
                .dontCatchUncaughtExceptions()
                .atMost(10, TimeUnit.SECONDS)
                .pollInterval(100, TimeUnit.MILLISECONDS)
                .until(future::isDone);

    }
}
